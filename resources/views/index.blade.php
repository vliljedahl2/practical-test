<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Frontpage</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- Styles -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    


    <body>

        <div class="flex-center position-ref full-height">
            <div class="content">

                <div class="title m-b-md">
                    API caller
                </div>

                <form onsubmit="return getMovie(event)">
                    Title: <input type="text" name="Title" id="movie-title" placeholder="title" required><br>
                    Year: <input type="number" name="Year" id="movie-year" placeholder="year" required><br>
                    Plot type: <select name="Plot" id="movie-plot">
                        <option value="short">short</option>
                        <option value="long">long</option>
                    </select><br>
                    <input type="submit" value="Submit"><br>
                </form>

                <br/>

                <form onsubmit="return getBook(event)">
                    <input type="text" name="Isbn" id="book-isbn" placeholder="ISBN" required><br>
                    <input type="submit" value="Submit">
                </form>

                <div id="response"></div>

            </div>
        </div>
    </body>
</html>

<script>


function getMovie(e) {
    e.preventDefault();
    token = getAuthToken();

    title = $("#movie-title").val();
    year = $("#movie-year").val();
    plot = $("#movie-plot").val();

    title = title.replace(/\s/g, "+");

    $("#response").empty();

    $.ajax({
        url: "api/getMovie",
        type: "GET",
        data: { 
            "title": title, 
            "year": year, 
            "plot": plot
        },
        
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', token);
        },

        success: function(response) {
            // response is a json object
            response = JSON.stringify(response);
            $("#response").append("<p>"+ response +"</p>");
        },

        error: function(response) {
            alert(response);
        }
    });

    return false;
}

function getBook(e) {
    e.preventDefault();
    token = getAuthToken();

    isbn = $("#book-isbn").val();

    isbn = isbn.replace("-", "");

    $("#response").empty();

    $.ajax({
        url: "api/getBook",
        type: "GET",
        data: { 
            "isbn": isbn
        },
        
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', token);
        },

        success: function(response) {
            response = JSON.stringify(response);
            $("#response").append("<p>"+ response +"</p>");
        },

        error: function(response) {
            alert(response);
        }
    });

    return false;
}

function getAuthToken() {
    var req = new XMLHttpRequest();
    req.open('GET', document.location, false);
    req.send(null);
    var header = req.getResponseHeader('authorization');
    return header;
}

</script>