<?php

class Response {
    
    private $content = [];

    function __construct($content) {
        $this->content = $this->parseContent($content);
    }

    public function getContent() {
        return $this->content;
    }

    public function hasErrors() {
        return ($this->content['success'] == 'false') ? true : false;
    }

    public function getErrorMessage() {
        return $this->content['error'];
    }

    private function parseContent($content) {
        $content = json_decode($content, true);
        if ($content == null) { 
            return array(); 
        }
        return $content;
    }
}

?>