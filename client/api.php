<?php

class Api {

    private $root_url;
    private $api_key;

    function __construct($root_url, $api_key) {
        $this->root_url = $root_url;
        $this->api_key = $api_key;
    }

    public function getBook($isbn_code) {
        $curl = curl_init("$this->root_url/api/getBook/?isbn=$isbn_code");
        return $this->executeCurl($curl);
    }

    public function getMovie($args) {
        $curl = curl_init("$this->root_url/api/getMovie/?title={$args['title']}&year={$args['year']}&plot={$args['plot']}");
        return $this->executeCurl($curl);
    }

    public function executeCurl($curl) {
        curl_setopt_array($curl, array(
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer $this->api_key"
            )
        ));

        $curlResponse = curl_exec($curl);
        $curlError = curl_error($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        
        if ($curlError) {
            throw new Exception('CurlERROR: ' . $curlError);
		}
		
        return new Response($curlResponse);
    }
}

?>