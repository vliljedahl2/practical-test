<?php

include('configs.php');

$title = isset($_POST['movie_title']) ? $_POST['movie_title'] : '';
$year = isset($_POST['movie_year']) ? $_POST['movie_year'] : '';
$plot = isset($_POST['movie_plot']) ? $_POST['movie_plot'] : '';

if (strpos($title, ' ') !== false) {
    $title = str_replace(" ","%20", $title);
}

$args = [
    'title' => $title,
    'year' => $year,
    'plot' => $plot
];

$api = new Api(ROOT_URL, API_KEY);
$response = $api->getMovie($args);

var_dump($response->getContent());

?>