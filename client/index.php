<html>

<h2>GET Movie</h2>

    <form action="/get_movie.php" method="post">
        <table>
            <tr>
			    <th>Get movie</th>
			    <th></th>
		    </tr>
		    <tr>
                <td><label for="movie_title">Movie title</label></td>
                <td><input name="movie_title" id="movie_title" type="text" required></td>
		    </tr>
            <tr>
                <td><label for="movie_year">Movie year</label></td>
                <td><input name="movie_year" id="movie_year" type="number" required></td>
		    </tr>
            <tr>
                <td><label for="movie_plot">Plot type</label></td>
                <td><select name="movie_plot" id="movie_plot" required>
                    <option value="short">short</option>
                    <option value="long">long</option>
                </select></td>
		    </tr>
            
            <tr>
                <td><input value="submit" type="submit" value="Get movie"></td>
            </tr> 
        </table>
    </form>

    <h2>GET Book</h2>

    <form action="/get_book.php" method="post">
        <table>
            <tr>
                <th>Get book</th>
                <th></th>
            </tr>
            <tr>
                <td><label for="isbn_code">Book isbn</label></td>
                <td><input name="isbn_code" id="isbn_code" type="text" required></td>
            </tr> 

            <tr>
                <td><input value="submit" type="submit" value="Get book"></td>
            </tr> 
        </table>
    </form>
    
</html>