<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Api\MovieApi;
use App\Api\BookApi;
use Tests\TestRepository;
use App\Exceptions\InvalidParametersException;
use Exception;

class ExampleTest extends TestCase
{
    public function testMovieApiReturnsAnJson() {
        $repository = new TestRepository();
        $api = new MovieApi($repository);
        
        $parameters = array(
            "title" => "movie",
            "year" => 1995,
            "plot" => "short"
        );

        $result = $api->call($parameters);

        $this->assertInternalType('array', $result);
    }

    public function testMovieApiRaisesErrorWhenEmptyArrayGiven() {
        $this->expectException(InvalidParametersException::Class);
        
        $repository = new TestRepository();
        $api = new MovieApi($repository);
        $parameters = array();
        $result = $api->call($parameters);
    }

    public function testImageApiReturnsAnJson() {
        $repository = new TestRepository();
        $api = new BookApi($repository);
        
        $isbn = '12345678';

        $result = $api->call($isbn);

        $this->assertInternalType('array', $result);
    } 

    public function testBookApiRaisesErrorWhenEmptyIsbnGiven() {
        $this->expectException(InvalidParametersException::Class);
        
        $repository = new TestRepository();
        $api = new BookApi($repository);
        $isbn = "";
        $result = $api->call($isbn);
    }
}
