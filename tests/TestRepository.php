<?php

namespace Tests;

use App\Api\IRepository;

class TestRepository implements IRepository {

    public function execute($parameters) {
        return '{
            "testjson": {
                "foo": "bar"
            }
        }';
    }

}

?>