<?php

namespace App\Api;

use App\Api\ApiClass;
use App\Exceptions\InvalidParametersException;
use Log;

class BookApi extends ApiClass {

    public function validateParameters($isbn) {
        /*
         * Here we can check that all the parameters are valid.
         */
        if (empty($isbn)) {
            throw new InvalidParametersException('Request cannot be empty.');
        }
    }

    public function parseResponse($response) {
        /**
         * If we wan't we could parse the response here 
         * to be more appropriate and useful.
         */
        $stringPrefix = "var _OLBookInfo = ";
        
        if (strpos($response, $stringPrefix) !== false) {
            $response = str_replace($stringPrefix, "", $response);
        }

        if (strpos($response, ';') !== false) {
            $response = str_replace(";", "", $response);
        }

        return json_decode($response, true);
    }
}

?>