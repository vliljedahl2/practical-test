<?php

namespace App\Api;

interface IRepository {
    /**
     * Interface for the repositories used by api classes.
     * Execute method makes the call to the repository.
     * 
     * Api class needs an implementation of this Interface and
     * returns a response to the api class.
     */
    public function execute($parameters);
}

?>