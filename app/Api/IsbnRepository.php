<?php

namespace App\Api;

use App\Api\IRepository;
use Exception;
use Log;

class IsbnRepository implements IRepository {

    public function __construct() {}

    public function execute($parameters) {

        $curl = curl_init("https://openlibrary.org/api/books?bibkeys=ISBN:$parameters");
        curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
        ));

        $response = curl_exec($curl);
        $curlError = curl_error($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        
        curl_close($curl);

        if ($curlError) {
            throw new Exception('CurlERROR: ' . $curlError);
        }

        return $response;
    }
}

?>