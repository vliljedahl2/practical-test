<?php

namespace App\Api;

use App\Api\IRepository;
use Exception;
use Log;

class OmdbRepository implements IRepository {

    // TODO: move this into application settings
    private $secret_key;

    public function __construct() {
        $this->secret_key = env("OMDB_API_KEY", "");
    }

    public function execute($parameters) {
        $title = $parameters['title'];
        $year = $parameters['year'];
        $plot = $parameters['plot'];

        $curl = curl_init("http://www.omdbapi.com/?apikey=$this->secret_key&t=$title&y=$year&plot=$plot");
        curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
        ));

        $response = curl_exec($curl);
        $curlError = curl_error($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($curlError) {
            throw new Exception('CurlERROR: ' . $curlError);
        }

        return $response;
    }
}

?>