<?php

namespace App\Api;

use App\Api\IRepository;

abstract class ApiClass {
    /**
     * Abstract class for api calls outside of this application.
     * Extend this class to create new api call.
     * 
     * Based on Template Pattern
     */
    private $repository;

    public function __construct(IRepository $repository) {
        $this->repository = $repository;
    }

    public function call($parameters) {
        $this->validateParameters($parameters);

        // If Curl error is detected.
        try {
            $response = $this->repository->execute($parameters);
        } catch (Exception $e) {
            throw $e;
        }

        return $this->parseResponse($response);
    }

    abstract function validateParameters($parameters);
    
    abstract function parseResponse($response);
}

?>