<?php

namespace App\Api;

use App\Api\ApiClass;
use App\Exceptions\InvalidParametersException;

class MovieApi extends ApiClass {

    public function validateParameters($parameters) {
        /**
         * Here we can check that all the parameters are valid.
         */
        if (empty($parameters)) {
            throw new InvalidParametersException('Request cannot be empty.');
        }
    }

    public function parseResponse($response) {
        /**
         * If we wan't we could parse the response here 
         * to be more appropriate and useful.
         */
        return json_decode($response, true);
    }
}

?>