<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Log;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            // Token is the Authorization header from request without 'Bearer'.
            $token = JWTAuth::getToken();

            // Validate the JWT token
            $apy = JWTAuth::getPayload($token)->toArray();
            
        } catch (Exception $e) {
            // TODO: Catch specific exceptions.
            return response()->json([
                'success' => 'false',
                'payload' => '',
                'error' => 'Token is invalid'
            ]);
        }

        $response = $next($request);
        $response->headers->set('Authorization', 'Bearer '.$token);

        return $response;
    }
}
