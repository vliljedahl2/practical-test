<?php

namespace App\Http\Middleware;

use Closure;
use JWTFactory;
use JWTAuth;

class CreateJWToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $jwtFactory = JWTFactory::customClaims([
            'sub' => env('APP_KEY'),
        ]);

        $payload = $jwtFactory->make();
        $token = JWTAuth::encode($payload);
        
        $response->headers->set('Authorization', 'Bearer '.$token);
        return $response;
    }
}
