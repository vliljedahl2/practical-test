<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api\MovieApi;
use App\Api\BookApi;
use App\Api\OmdbRepository;
use App\Api\IsbnRepository;
use App\Exceptions\InvalidParametersException;
use JWTAuth;
use JWTFactory;
use Log;

class ApiController extends Controller
{
    
    /**
     * FOR TEST PURPOSES ONLY!
     * 
     * Returns the api key, if anyone wants to use the API
     */
    public function getApiKey(Request $request) {

        /**
         * We're using the app_key to generate the JWToken.
         * We could as well use an user from laravel for this.
         */ 
        $jwtFactory = JWTFactory::customClaims([
            'sub' => env('APP_KEY'),
        ]);

        $payload = $jwtFactory->make();
        $token = JWTAuth::encode($payload);

        return response()->json(["token" => "$token"]);
    }

    public function getMovie(Request $request) {
        $title = $request->input('title');
        $year = $request->input('year');
        $plot = $request->input('plot');

        if (strpos($title, ' ') !== false) {
            $title = str_replace(" ","%20", $title);
        }

        $parameters = [
            "title" => $title,
            "year" => $year,
            "plot" => $plot
        ];

        $repository = new OmdbRepository(); 
        $api = new MovieApi($repository);
        
        try {
            $response = $api->call($parameters);
        } catch (InvalidParametersException $e) {
            return $this->badRequestResponse($e);
        } catch (Exception $e) {
            return $this->badRequestResponse($e);
        }

        return $this->okResponse($response);
    }

    public function getBook(Request $request) {
        $isbn = $request->input('isbn');

        $repository = new IsbnRepository();
        $api = new BookApi($repository);
        
        try {
            $response = $api->call($isbn);
        } catch (InvalidParametersException $e) {
            return $this->badRequestResponse($e);
        } catch (Exception $e) {
            return $this->badRequestResponse($e);
        }

        return $this->okResponse($response);
    }

    private function errorResponse($message) {
        return response(json_encode([
            "success" => "false",
            "payload" => "",
            "error" => "$message"
        ]), 400);
    } 
    
    private function badRequestResponse($message) {
        return response(json_encode([
            "success" => "false",
            "payload" => "",
            "error" => "$message"
        ]), 400);
    }

    private function okResponse($response) {
        return response([
            "success" => "true",
            "payload" => $response
        ], 200);
    }
}
