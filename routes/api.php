<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/getApiKey', 'ApiController@getApiKey');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('/getMovie', 'ApiController@getMovie');
    Route::get('/getBook', 'ApiController@getBook');
});
