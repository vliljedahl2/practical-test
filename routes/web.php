<?php

Route::group(['middleware' => ['jwt.create']], function() {
    Route::get('/', 'FrontpageController@index');
});